import Vue from 'vue'
import Router from 'vue-router'
import Book from './views/Book.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/book',
      name: 'book',
      component: Book
    },
    {
      path: '/content',
      name: 'content',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "content" */ './views/Content.vue')
    },
    {
      path: '*',
      redirect: '/book'
    }
  ]
})
